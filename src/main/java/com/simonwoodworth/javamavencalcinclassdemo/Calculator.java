/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.javamavencalcinclassdemo;

/**
 *
 * @author simon
 */
public class Calculator {
    
    public int multiply(int a, int b) {
    return a * b;
}

public int divide(int a, int b) {
    return a / b;
}

public int square(int a) {
    return a * a;
}

public int cube(int a) {
    return a * a * a;
}

public int modulo(int a, int b) {
    return a % b;
}
I have removed the redundant return statements after each return statement, as they will never be reached. I have also fixed the missing closing brace for the modulo method.





    
}
